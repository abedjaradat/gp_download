var http = require('http');
var crypto = require('crypto');
var knox = require("knox");

http.createServer(function(req, res) {
  var path = req.url;
  if (path.includes("/modelFiles/")) {
    path = decodeURIComponent(path.split("/").pop());
    if (!path) return res.end(JSON.stringify({status: 404, message: "file not found"}));
    var key = "mxd3d@12345678901234567890123456";
    var enc64Var = new Buffer(key).toString('Base64');
    var md5Var = crypto.createHash('md5').update(key).digest('Base64');
    var s3BucketName = "gameprint.net";
    var client = knox.createClient({key: "AKIAI4SCWWYFEFN7SZWA", secret: "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", bucket: s3BucketName, region: "us-west-1"});
    var getFile = function(fileURL, decryptIt, callback) {
      var header = {};
      if (decryptIt) {
        header = {
          'x-amz-server-side-encryption-customer-key': enc64Var,
          'x-amz-server-side-encryption-customer-key-MD5': md5Var,
          'x-amz-server-side-encryption-customer-algorithm': 'AES256'
        };
      }
      client.getFile(fileURL, header, function(err, res) {
        callback(err, res);
      });
    }

    var fileName = path.split("/").pop();
    getFile(path, true, function(err, result) {
      if (err) {
        return res.end(JSON.stringify({status: 400, message: "file not found"}));
      } else {
        if (result.status == 404) {
          return res.end(JSON.stringify({status: 400, message: "file not found"}));
        } else {
          result.headers['Content-Disposition'] = `filename=${fileName}`;
          res.writeHead(200, result.headers);
          result.pipe(res);
        }
      }
    });
  } else {
    return res.end(JSON.stringify({status: 400, message: "file not found"}));
  }

}).listen(4060);
